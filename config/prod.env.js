'use strict'

module.exports = {
  NODE_ENV: '"production"',
  ENDPOINT: '"' + process.env.ENDPOINT + '"',
  API_URL: '"' + process.env.API_URL + '"'
}
