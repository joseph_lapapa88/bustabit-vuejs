const env = require('dotenv').load({ path: '.env' }).parsed

const mainApp = {
  name: 'play',
  script: './server.js',
  exec_mode: "cluster",
  instances: "max",
  env
}

const apps = [mainApp]

module.exports = { apps }
