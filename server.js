require('dotenv').load()

const express = require('express')
const compression = require('compression')

const fs = require('fs')
const https = require('https')
const http = require('http')

const app = express()
const isHttps = parseInt(process.env.HTTPS) === 1
let server = null

if (isHttps) {
  server = https.createServer({
    ca: fs.readFileSync(`${process.cwd()}/ssl/certificate.ca-bundle`),
    key: fs.readFileSync(`${process.cwd()}/ssl/server.key`),
    cert: fs.readFileSync(`${process.cwd()}/ssl/certificate.crt`)
  }, app)
} else {
  server = http.createServer(app)
}

app.use(compression())

app.use(express.static('dist', {
  etag: true,
  maxAge: '1y'
}))

server.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}`))
