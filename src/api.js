import axios from 'axios'
import { RESET, LAST_EVENT, CHAT, ERROR, GAME_JOIN, GAME_SYNC } from './store/mutation-types'
import { isNull, isFunction, uniqueId, isEmpty, isUndefined, noop
  , assign } from 'lodash'

const REFERRER_ID = 'java'
let ActionheroWebsocketClient = window.ActionheroWebsocketClient

ActionheroWebsocketClient.prototype.$axios = null
ActionheroWebsocketClient.prototype.say = function (room, message, callback) {
  if (isNull(this.id)) {
    throw new Error('Waiting to be connected.')
  }
  let listener = ''
  if (this.state === 'connected') {
    const { method } = message
    if (isUndefined(message.uid)) {
      message.uid = uniqueId()
    }
    if (isEmpty(method)) {
      throw new Error('Say method missing.')
    }
    listener = `_${message.uid}`
    if (message.uid === 0) {
      listener = `_${method}`
    }
    if (isEmpty(message.referrerId)) {
      message.referrerId = REFERRER_ID
    }
    this.once(listener, callback || noop)
    this.once(`${listener}:error`, callback || noop)
    if (typeof message !== 'string') {
      message = JSON.stringify(message)
    }
    this.send({event: 'say', room, message})
  } else {
    throw new Error('Not yet connected.')
  }
  return listener
}
ActionheroWebsocketClient.prototype.action = function (action, params = {}, callback) {
  if (isNull(this.id)) {
    throw new Error(`Waiting to be connected. ${action}`)
  }
  if (isFunction(params)) {
    callback = params
    params = {}
  }
  if (isEmpty(params.action)) {
    params.action = action
  }
  params.uid = uniqueId()
  if (isEmpty(params.referrerId)) {
    params.referrerId = REFERRER_ID
  }
  let listener = `_${params.uid}`
  if (this.state !== 'connected') {
    this.actionWeb(params, callback || noop)
  } else {
    // event handler
    this.once(listener, callback || noop)
    this.once(`${listener}:error`, callback || noop)
    this.actionWebSocket(params)
  }
}
ActionheroWebsocketClient.prototype.httpCaching = false
ActionheroWebsocketClient.prototype.enableCaching = function () {
  this.httpCaching = true
  return this
}
ActionheroWebsocketClient.prototype.disableCaching = function () {
  this.httpCaching = false
  return this
}
ActionheroWebsocketClient.prototype.configure = function (callback) {
  this.detailsView((details) => {
    if (details && details.data) {
      this.id = details.data.id
      this.fingerprint = details.data.fingerprint
      this.rooms = details.data.rooms
      callback(details)
    } else {
      this.disconnect()
    }
  })
}
ActionheroWebsocketClient.prototype.removeFromExistingRooms = function () {
  (this.rooms || []).forEach((room) => {
    this.roomLeave(room)
    this.rooms.splice(this.rooms.indexOf(room), 1)
  })
}
ActionheroWebsocketClient.prototype.actionWeb = function (params = {}, callback = noop) {
  if (isEmpty(this.$axios)) {
    const { params: { locale } } = this.$_store.state.ac
    const baseURL = `${process.env.API_URL}${process.env.ENDPOINT}`
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'X-Requested-With': true,
      'Accept-Language': locale
    }
    this.$axios = axios.create({ baseURL, headers, timeout: 1000 })
  }
  this.$axios.request({
    method: 'post',
    withCredentials: true,
    data: params
  }).then((resp) => {
    return callback(null, this.handleResponse(resp))
  }).catch((error) => {
    this.handleError(error)
    return callback(error)
  })
}
ActionheroWebsocketClient.prototype.defaultParams = function (params = {}) {
  const { params: { locale, token } } = this.$_store.state.ac
  var data = assign({}, params, { uid: uniqueId(), referrerId: REFERRER_ID, locale })
  if (isEmpty(data.token)) {
    data.token = token
  }
  return data
}
ActionheroWebsocketClient.prototype.handleResponse = function handleResponse ({ data }) {
  if (data) { // from web
    if (data.resp) { // api response
      const { httpStatusCode } = data.resp
      if (httpStatusCode !== 200) {
        this.$_store.commit(ERROR, data.resp)
        console.log(data)
      } else {
        return data.resp
      }
    } else {
      this.$_store.commit(ERROR, { message: 'Unknown API Response' })
      console.log(data)
    }
  }
  return {}
}
ActionheroWebsocketClient.prototype.handleError = function handleError (error) {
  if (error.response) {
    const { data } = error.response
    if (data && data.resp) { // error from api
      const { code } = data.resp
      // handle api codes
      if ([18].indexOf(code) >= 0) {
        this.$_store.commit(ERROR, {
          ...data.resp,
          type: 'info.blocking|noRefresh'
        })
        return
      }
    } else {
      console.log(error.response)
    }
    console.log('validation or user error')
  } else if (error.request) {
    console.log('validation or user error')
    console.log(error.request)
  } else {
    if (isEmpty(error.type)) {
      error.type = 'info.blocking'
    }
    this.$_store.commit(ERROR, error)
  }
}
ActionheroWebsocketClient.prototype.authenticate = function authenticate (payload) {
  return new Promise((resolve, reject) => {
    const params = this.defaultParams({
      action: 'auth:play',
      ...payload
    })
    this.actionWeb(params, (err, resp) => {
      if (err) {
        return reject(err)
      }
      const { data } = resp
      if (isEmpty(data.currency)) {
        return reject(new Error('Undefined currency.'))
      }
      return resolve(data)
    })
  })
}
ActionheroWebsocketClient.prototype.joinGame = function joinGame (currency) {
  const gameRoom = `${currency}GameRoom`
  return new Promise((resolve, reject) => {
    this.once(`${gameRoom}:joined`, (message) => {
      if (message.error) {
        this.handleError(message.error)
        return reject(new Error(message.error.message))
      }
      return resolve(message)
    })
    this.once(`${gameRoom}:leaved`, () => {
      this.roomAdd(gameRoom)
    })
    this.roomLeave(gameRoom)
  })
}
ActionheroWebsocketClient.prototype.authGame = function authGame (currency) {
  const gameRoom = `${currency}GameRoom`
  const message = this.defaultParams({ method: 'room:authenticate' })
  return new Promise((resolve, reject) => {
    this.once(`${gameRoom}:authenticated`, (message) => {
      if (message.error) {
        this.handleError(message.error)
        return reject(new Error(message.error.message))
      }
      return resolve(message)
    })
    this.say(gameRoom, message)
  })
}

window.onbeforeunload = () => {
  if (window.awc) {
    window.awc.removeFromExistingRooms()
  }
}
window.onunload = () => {
  if (window.awc) {
    window.awc.removeFromExistingRooms()
  }
}
window.onabort = () => {
  if (window.awc) {
    window.awc.removeFromExistingRooms()
  }
}

function awcListener () {
  this.on('error', function (error) {
    if (error) {
      console.error(error)
    }
    console.log('error')
    this.$_store.commit(LAST_EVENT, 'error')
  })

  this.client.on('error', function (error) {
    console.log(error)
  })

  this.on('reconnect', function () {
    console.log('reconnect')

    this.$_store.commit(LAST_EVENT, 'reconnect')
    this.handleError({ type: 'info.blocking|noRefresh', message: 'Disconnected... Trying to reconnect now.' })
  })

  this.on('reconnecting', function () {
    console.log('reconnecting')
    this.$_store.commit(LAST_EVENT, 'reconnecting')
  })

  this.on('connected', function () {
    console.log('connected!')

    this.$_store.commit(LAST_EVENT, 'connected')
    if (this.$_store.state.ac.prevEvent === 'reconnect') {
      this.handleError({ type: 'success.closeIn:3500', message: 'Reconnected successfully!' }) // close the modal
    }
  })

  this.on('disconnected', function () {
    console.log('disconnected :(')

    this.$_store.commit(LAST_EVENT, 'disconnected')
    this.handleError({ type: 'warning.blocking|noRefresh', message: 'Disconnected from the server.' })

    this.$_store.commit(RESET)
    this.connect() // try to reconnect
  })

  this.on('timeout', function () {
    console.log('timeout')

    this.$_store.commit(LAST_EVENT, 'timeout')
    this.handleError({ type: 'warning.blocking|noRefresh', message: 'Server is unreachable as of this moment. Please contact support or try to manually reload the page.' })
  })

  this.on('welcome', function (message) {
    console.log(message)
  }) // server welcome message

  this.on('say', function (message = {}) {
    // response fingerprint
    // room:authenticate: "{from, message: { locale, method: 'room:authenticate', referrerId, token, uid}, room, sentAt}"
    if (message.message) {
      const { method } = message.message
      if (method === 'room:authenticate') {
        return this.emit(`${message.room}:authenticated`, message.message)
      }
      if (method === 'game:sync') {
        const { data } = message.message
        return this.$_store.commit(GAME_SYNC, data)
      }
    }
    console.log(`say ${JSON.stringify(message)}`)
  }) // say messages from rooms actionhero is in

  this.on('message', function (message = {}) {
    if (message.context === 'user') {
      return
    }

    // response fingerprint
    // roomAdd: "{"status":"OK","context":"response","data":true,"verb":"roomAdd","room":"usdGameRoom","messageCount":2}"
    // chat:update: "{"context":"response","message":{chat:[], connectionId, method, room}}"
    if (message.context === 'response') {
      if (message.verb) {
        if (message.verb === 'roomAdd') {
          return this.emit(`${message.room}:joined`, message)
        }
        if (message.verb === 'roomLeave') {
          return this.emit(`${message.room}:leaved`, message)
        }
        if (message.verb === 'say' || message.verb === 'detailsView') {
          return
        }
      }

      if (message.message) {
        const { method } = message.message
        if (method === 'chat:update') {
          const { chat } = message.message
          return this.$_store.commit(CHAT, chat)
        }
        if (method === 'game:join') {
          const { data } = message.message
          return this.$_store.commit(GAME_JOIN, data)
        }
      }
    }

    console.log(`message ${JSON.stringify(message)}`)
  }) // all messages (noisy)
}

export default function createApi (store) {
  window.awc = new ActionheroWebsocketClient({
    apiPath: process.env.ENDPOINT,
    url: process.env.API_URL
  })

  // attach store on api
  window.Object.defineProperty(window.awc, '$_store', {
    get: () => store
  })

  // attach api on store
  window.Object.defineProperty(store, '$_api', {
    get: () => window.awc
  })

  window.awc.connect(function (error) {
    if (error) {
      this.handleError({ type: 'warning.blocking|noRefresh', message: 'Disconnected from the server.' })
    }
  }.bind(window.awc))

  awcListener.call(window.awc)
}
