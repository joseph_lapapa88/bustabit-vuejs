// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// import 3rd party js
import jquery from '&/jquery'
import wNumb from '&/wNumb'

import { isEmpty, find, isPlainObject } from 'lodash'

import Vue from 'vue'
import App from './App'
import SiteMixin from './plugins/SiteMixin'
import ActionHero from './plugins/ActionHero'
import router from './router'
import store from './store'
import { i18n } from './lang/i18n-setup'

// grid system
import VueFractionGrid from 'vue-fraction-grid'

// icon
import 'vue-awesome/icons/expand'
import 'vue-awesome/icons/cog'
import 'vue-awesome/icons/clipboard'
import Icon from 'vue-awesome/components/Icon'

// set some globals
window.$ = jquery // jquery alias

Vue.use(VueFractionGrid, {
  gutter: '0px',
  approach: 'mobile-first',
  breakpoints: {
    small: '320px 479px',
    mobile: '480px 767px',
    tablet: '768px 1023px',
    desktop: '1024px'
  }
})

Vue.component('icon', Icon)

Vue.use(SiteMixin)
Vue.use(ActionHero)

Vue.config.productionTip = true

// filters
Vue.filter('amount', (value, prefix = '', suffix = '') => {
  let amount = value
  if (Array.isArray(value) && !isEmpty(prefix)) {
    amount = (find(amount, { currency: prefix }) || {}).amount || 0
  } else if (isPlainObject(value)) {
    amount = value.amount || 0
  }

  if (isNaN(amount)) {
    console.warn(`Not a number. ${amount}`)
    amount = 0
  }

  const format = wNumb({
    mark: '.',
    thousand: ',',
    prefix: `${prefix} `,
    suffix
  })
  return format.to(amount)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  i18n,
  components: { App },
  destroy () {
    // remove window resize listener
    window.removeEventListener('resize', this.$_handleResize())
  },
  created () {
    // attache resize event listener
    window.addEventListener('resize', this.$_handleResize())
  },
  template: '<App/>'
})
// Hot updates
if (module.hot) {
  module.hot.accept(['@/lang/en', '@/lang/ja'], function () {
    i18n.setLocaleMessage('en', require('@/lang/en').default)
    i18n.setLocaleMessage('ja', require('@/lang/ja').default)
  })
}
