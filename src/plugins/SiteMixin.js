'use strict'

import { debounce } from 'lodash'
import { THEME, FULL_SCREEN } from '../store/mutation-types'

const SiteMixin = {
  install (Vue) {
    Vue.prototype.$_resizeHandlers = []
    Vue.prototype.$_handleResize = function handleResize () {
      return debounce(() => {
        this.$_resizeHandlers.forEach(fn => fn())
      }, 150)
    }

    Vue.mixin({
      // global computed properties
      computed: {
        isCompact () {
          return (this.viewPortLabel === 'mobile' || this.viewPortLabel === 'small')
        },
        // property for getting the current match classes for current viewport
        mobileClass () {
          return this.$store.state.site.classes
        },
        // property for getting the current view port name
        viewPortLabel () {
          return this.$store.state.site.currentMatch.label
        },
        // property for getting the expand mode
        isFullScreen () {
          return this.$store.state.site.fullScreen
        },
        // property for getting the current theme
        isBlack () {
          return this.$store.state.site.theme === 'black'
        },
        isWhite () {
          return this.$store.state.site.theme === 'white'
        },
        sizes () {
          return {
            widthMultiplier: 0.648,
            heightMultiplier: 0.45,
            graphFormMultiplier: 0.61,
            graphFormMultiplierOnCompact: 0.71
          }
        },
        remainingHeight () {
          return this.$store.getters.remainingHeight
        }
      },
      methods: {
        toggleExpandMode () {
          this.$store.commit(FULL_SCREEN)
        },
        toggleTheme () {
          this.$store.commit(THEME)
        }
      }
    })
  }
}

export default SiteMixin
