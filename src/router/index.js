import Vue from 'vue'
import { loadLanguageAsync } from '../lang/i18n-setup'
import Router from 'vue-router'
import NotFoundPage from '@/components/NotFoundPage'
import HomePage from '@/components/HomePage'
import PlayGamePage from '@/components/PlayGamePage'
import UserPage from '@/components/UserPage'
import PlayInfoPage from '@/components/PlayInfoPage'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    }, {
      path: '/:fingerprint/:token/:play/:locale',
      name: 'PlayGamePage',
      component: PlayGamePage
    }, {
      path: '/play/:id',
      name: 'PlayInfoPage',
      component: PlayInfoPage
    }, {
      path: '/player/:id',
      name: 'UserPage',
      component: UserPage
    }, {
      path: '*',
      name: 'NotFoundPage',
      component: NotFoundPage
    }
  ]
})

router.beforeEach((to, from, next) => {
  const lang = to.params.locale || 'en'
  loadLanguageAsync(lang).then(() => next())
})

export default router
