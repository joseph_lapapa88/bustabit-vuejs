import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import createApi from '../api'
import site from './modules/site'
import ac from './modules/ac'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'
const plugins = debug ? [createLogger()] : []

const store = new Vuex.Store({
  modules: { site, ac },
  strict: debug,
  plugins
})

createApi(store)

window.$store = store

export default store
