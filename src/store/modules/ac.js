import moment from 'moment'
import { RESET, CHAT, CONNECTION, ERROR, PROFILE, LAST_EVENT, BALANCE_SYNC } from '../mutation-types'
import isPlainObject from 'lodash/isPlainObject'
import assign from 'lodash/assign'
import game from './game'

const state = {
  state: '',
  gameState: 'disconnected',
  username: '--',
  balance: -10000000,
  currency: '--',
  lastEvent: null,
  prevEvent: null,
  error: {
    code: 0,
    httpStatusCode: 0,
    message: '',
    type: ''
  },
  params: {
    fingerprint: '',
    token: '',
    play: '',
    locale: 'en'
  },
  chat: []
}

// getters
const getters = {}

// actions
const actions = {
  authenticate ({ dispatch, commit, state: { state } }, payload) {
    // authenticate the connection
    this.$_api.authenticate(payload).then((data) => {
      const { currency, info } = data
      commit(CONNECTION, { params: payload, currency })
      commit(PROFILE, info)

      dispatch('joinGame', { currency })
    })
  },
  balanceSync ({ commit }, amount) {
    commit(BALANCE_SYNC, amount)
  }
}

// mutations
const mutations = {
  [CONNECTION] (state, payload) {
    if (isPlainObject(payload)) {
      assign(state, payload)
    }
  },
  [LAST_EVENT] (state, eventName) {
    state.prevEvent = state.lastEvent
    state.lastEvent = eventName
    // update ac.state based on connection related last event
    if (eventName === 'connected') {
      state.state = 'connected'
    }
  },
  [PROFILE] (state, { username, balance }) {
    state.username = username
    state.balance = balance
  },
  [CHAT] (state, payload = []) {
    let chat = payload
    state.chat = chat.map((item) => {
      const i = JSON.parse(item)
      i.atMoment = moment.utc(i.at)
      i.at = i.atMoment.local()
      i.x = i.atMoment.unix()
      i.atTime = i.atMoment.format('HH:mm')
      return i
    })
  },
  [BALANCE_SYNC] (state, { amount }) {
    state.balance += amount
  },
  [ERROR] (state, { type = '', code = 0, httpStatusCode = null, message = '' }) {
    state.error = { code, httpStatusCode, message, type }
  },
  [RESET] (state) {
    assign(state, {
      gameState: 'disconnected',
      state: 'disconnected',
      currency: '--',
      username: '--',
      balance: -10000000
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  modules: { game }
}
