import { PROFILE, PLAY_SYNC, BALANCE_SYNC, RESET, GAME_SYNC, GAME_JOIN } from '../mutation-types'
import assign from 'lodash/assign'
import isString from 'lodash/isString'
import isEmpty from 'lodash/isEmpty'
import isNumber from 'lodash/isNumber'
import sortBy from 'lodash/sortBy'
import find from 'lodash/find'
import filter from 'lodash/filter'
import first from 'lodash/first'

function getBonusPercentEq ({ bonus, bet }) {
  if (bonus > 0) {
    return `${((bonus / bet) * 100).toFixed(2)}%`
  }
  return '-'
}

function calcProfit ({ bonus, won }) {
  if (won >= 0) {
    return (bonus + won).toFixed(2)
  }
  return '-'
}

function beautifyBets (bets) {
  if (isString(bets)) {
    return {
      username: bets,
      bet: '-',
      bonus: 0,
      bonusPercent: '-',
      stoppedAt: 0,
      stoppedAtStr: '-',
      won: 0,
      profit: '-'
    }
  }
  const newBets = bets.map((item) => {
    const [username, bet, autoCashOut, stoppedAt, status, won, bonus] = item
    let bonusPercent = getBonusPercentEq({ bonus, bet })
    const profit = calcProfit({ bonus, won })
    let stoppedAtDec = parseFloat(((stoppedAt || 0) * this.multiplier).toFixed(2))
    const stoppedAtStr = stoppedAtDec > 0 ? `${stoppedAtDec}x` : '-'
    return {
      username,
      bet,
      bonus,
      bonusPercent,
      stoppedAt: stoppedAtDec,
      stoppedAtStr,
      won,
      profit,
      autoCashOut,
      status
    }
  })
  return sortBy(newBets, (item) => {
    return item.stoppedAt ? -item.stoppedAt : null
  })
}

function calcBonuses (bets = []) {
  if (!Array.isArray(bets)) {
    throw new Error('Expecting the parameter to be array.')
  }

  if (isEmpty(bets)) {
    return bets
  }

  let bonusPool = 0
  let largestBet = 0

  const slideSameStoppedAt = (arr, fn) => {
    let i = 0
    while (i < arr.length) {
      const tmp = []
      let betAmount = 0
      let sa = arr[i].stoppedAt
      for (; i < arr.length && arr[i].stoppedAt === sa; ++i) {
        betAmount += arr[i].bet
        tmp.push(arr[i])
      }
      if (tmp.length >= 1) {
        fn(tmp, sa, betAmount)
      }
    }
  }

  let newBets = bets.map((item) => {
    bonusPool += item.bet * 0.01
    largestBet = Math.max(largestBet, item.bet)
    if (isNumber(item.won)) {
      item.won = parseFloat(item.won.toFixed(2))
    }
    item.bonus = 0
    item.bonusInPercent = '0%'
    item.stoppedAtStr = '-'
    if (item.stoppedAt > 0) {
      item.stoppedAtStr = `${item.stoppedAt}x`
    }
    return item
  })

  // const pending = filter(newBets, (i) => i.won < 0 || isString(i.won))
  const sorted = sortBy(newBets, (i) => {
    if (i.won > 0) {
      return i.stoppedAt * -1
    } else {
      return null
    }
  })

  newBets = sorted.concat([])

  if (this.game.state === 'started' || this.game.state === 'starting') {
    return newBets
  }

  const maxWinRatio = bonusPool / largestBet

  slideSameStoppedAt(sorted, (listOfRecords, cashOutAmount, totalBetAmount) => {
    if (bonusPool <= 0) {
      return
    }
    const toAllocAll = Math.min(totalBetAmount * maxWinRatio, bonusPool)
    listOfRecords.forEach(({ username, bet }) => {
      const toAlloc = (bet / totalBetAmount) * toAllocAll
      if (toAlloc >= 0) {
        const entry = find(newBets, { username })
        bonusPool -= toAlloc
        entry.bonus = parseFloat(toAlloc.toFixed(2))
        entry.profit = calcProfit(entry)
        if (entry.bonus > 0) {
          entry.bonusInPercent = getBonusPercentEq(entry)
        }
      }
    })
  })

  const a = filter(newBets, (i) => i.stoppedAt > 0)
  const b = filter(newBets, (i) => i.stoppedAt <= 0)

  return b.concat(a)
}

function parseGameData (payload = {}) {
  // process only if the page is for gaming
  let { join } = payload

  if (join) {
    this.game.state = payload.state
    this.game.nyan = payload.nyan
    this.game.at = payload.at.toFixed(2)
    this.game.maxWin = payload.maxWin
    this.game.bonusPercent = 0.01 // TOOD: should be get from server
    this.game.bet = payload.bet
    this.game.startTime = +(new Date(payload.startTime))
    this.game.tillTime = payload.tillTime > 0 ? payload.tillTime : this.game.startTime + 1100

    this.game.bets = beautifyBets.call(this, payload.bets)
    this.game.bets = calcBonuses.call(this, this.game.bets)

    // set the play state to idle
    this.play.state = 'idle'
    // item [username, bet, autoCashOut, stoppedAt, status, won, bonus]
    if (!isEmpty(this.game.bet)) {
      this.game.bet = first(beautifyBets.call(this, [this.game.bet]))
      this.play.manualData.bet = this.game.bet.bet
      this.play.manualData.autoCashOut = parseFloat((this.game.bet.autoCashOut * this.multiplier).toFixed(2))
      this.play.state = 'betting'
    } else {
      const item = find(this.game.bets, { username: this.username })
      if (!isEmpty(item)) {
        this.game.bet = item
        this.play.manualData.bet = item.bet
        this.play.manualData.autoCashOut = parseFloat((item.autoCashOut * this.multiplier).toFixed(2))
        this.play.state = 'playing'
        if (item.status === 'bet-cashed-out') {
          this.play.state = 'idle'
        }
        // TODO: call for user profile
      }
    }
    return this
  }

  const [state, at] = payload.meta
  const meta = payload.meta
  let username
  let index
  let stoppedAt
  let won
  switch (state) {
    case 'playing':
      this.game.state = state
      this.game.at = at.toFixed(2)
      this.game.nyan = meta[2]
      break
    case 'busted':
      this.game.state = state
      this.game.at = at.toFixed(2)
      this.game.nyan = false
      this.game.bets = calcBonuses(beautifyBets(meta[2]))
      const userBet = find(this.game.bets, { username: this.username })
      if (userBet) {
        // TODO: call for user profile
      }
      if (this.play.state === 'playing') {
        this.play.state = 'idle'
      }
      break
    case 'starting':
      this.game.state = state
      this.game.nyan = false
      this.game.maxWin = meta[1]
      this.game.tillTime = meta[2]
      this.game.startTime = Date.now()
      this.game.bet = {}
      break
    case 'started':
      this.game.state = state
      this.game.bets = beautifyBets.call(this, meta[1])
      break
    case 'bet':
      [, username, index] = meta
      if (this.game.bets[0] && this.game.bets[0].stoppedAt !== '-') {
        this.game.bets = []
      }
      if (username === this.username) {
        this.play.state = 'playing'
        // TODO: call for user profile
      }
      this.game.bets[index] = beautifyBets.call(this, username)
      break
    case 'cancel':
      this.play.state = 'idle'
      this.game.bet = {}
      break
    case 'cashed':
      [, username, stoppedAt, won] = meta
      if (username === this.username) {
        this.play.state = 'idle'
        // TODO: call for user profile
        if (this.play.autoStart) {
          this.play.autoData.result = won
        }
        // TODO: call for cashed
      }
      const item = find(this.game.bets, { username })
      if (item) {
        item.won = won
        item.stoppedAt = parseFloat((this.multiplier * stoppedAt).toFixed(2))
      }
      this.game.bets = calcBonuses(this.game.bets)
      break
  }
  return this
}

const state = {
  currency: '',
  authenticated: false,
  joined: false,
  multiplier: 0.01,
  username: '--',
  balance: -10000000,
  game: {
    state: 'lag',
    nyan: 1000000000,
    at: 0.0,
    maxWin: 1000000000,
    bonusPercent: 0.01,
    bet: {
      bet: 0, autoCashOut: 0
    },
    startTime: +(new Date()),
    tillTime: null,
    bets: []
  },
  play: {
    state: 'idle',
    mode: 'manual',
    bet: 0,
    autoCashOut: 0,
    autoStart: false,
    manualData: {
      bet: 2, autoCashOut: 2.00
    },
    autoData: {
      bet: 0,
      base: 2,
      autoCashOut: 2.5,
      stopBettingAt: 10,
      onLoss: {
        choice: 'returnToBaseBet',
        increaseBy: 2
      },
      onWin: {
        choice: 'returnToBaseBet',
        increaseBy: 2
      }
    }
  }
}

// getters
const getters = {}

// actions
const actions = {
  joinGame ({ dispatch, commit }, { currency }) {
    commit(GAME_SYNC, { currency })
    this.$_api.joinGame(currency).then(() => {
      dispatch('authGame')
    })
  },
  authGame ({ commit, state }) {
    this.$_api.authGame(state.currency).then(() => {
      commit(GAME_SYNC, { authenticated: true })
    })
  },
  placeBet ({ commit, dispatch, state }, { amount, autoCashOut }) {
    // call for place bet, commit bet details
    // if success, dispatch balanceSync
  },
  cancelBet ({ commit, dispatch, state }) {
    // call for cancel bet
    // if success, dispatch balanceSync and commit bet details
  },
  cashOut ({ commit, dispatch, state }) {
    // call for cashout
    // if success, dispatch balanceSync and commit bet details
  },
  stopAuto ({ commit, dispatch, state }) {
    // call for possible cancellation
    // commit play details
  },
  startAuto ({ commit, dispatch, state }) {
    // commit play details
  }
}

// mutations
const mutations = {
  [GAME_SYNC] (state, payload = {}) {
    if (typeof payload.meta !== 'undefined') {
      return parseGameData.call(state, payload)
    }
    assign(state, payload)
  },
  [GAME_JOIN] (state, payload) {
    state.multiplier = payload.multiplier
    parseGameData.call(state, payload)
  },
  [RESET] (state) {
    assign(state, {
      username: '--',
      currency: '--',
      balance: -10000000,
      joined: false,
      authenticated: false
    })
  },
  [PROFILE] (state, { username, balance }) {
    state.username = username
    state.balance = balance
  },
  [PLAY_SYNC] (state) {},
  [BALANCE_SYNC] (state) {}
}

export default {
  state,
  getters,
  actions,
  mutations
}
