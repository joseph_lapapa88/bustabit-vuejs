import { WINDOW_RESIZE, THEME, FULL_SCREEN, SIZES } from '../mutation-types'
import { orderBy, reverse, assign } from 'lodash'

const state = {
  currentMatch: {},
  classes: {},
  rules: [
    {
      rule: 320,
      label: 'small'
    },
    {
      rule: 480,
      label: 'mobile'
    },
    {
      rule: 768,
      label: 'tablet'
    },
    {
      rule: 1024,
      label: 'desktop'
    }
  ],
  theme: 'white',
  fullScreen: false,
  graphMode: 'canvas', // canvas or text
  sizes: {
    header: 100,
    page: 100
  }
}

// getters
const getters = {
  remainingHeight: ({ sizes }) => {
    const { page, header } = sizes
    return page - header
  }
}

// actions
const actions = {}

// mutations
const mutations = {
  [WINDOW_RESIZE] (state) {
    const width = window.innerWidth
    orderBy(state.rules, ['rule', 'desc']).forEach((item) => {
      if (width > item.rule) {
        state.currentMatch = item
      }
    })
    state.classes = {}
    if (state.currentMatch) {
      reverse(state.rules).forEach((item) => {
        if (state.currentMatch.rule <= item.rule) {
          state.classes[item.label] = true
        }
      })
    }
  },
  [THEME] (state) {
    state.theme = state.theme === 'white' ? 'black' : 'white'
  },
  [FULL_SCREEN] (state) {
    state.fullScreen = !state.fullScreen
  },
  [SIZES] (state, payload = {}) {
    assign(state.sizes, payload)
  }
}

export default {
  namespace: true,
  state,
  getters,
  actions,
  mutations
}
