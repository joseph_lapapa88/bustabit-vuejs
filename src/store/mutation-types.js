// for module site
export const WINDOW_RESIZE = 'WINDOW_RESIZE'
export const THEME = 'THEME'
export const FULL_SCREEN = 'FULL_SCREEN'
export const SIZES = 'SIZES'

// for module ac(actionhero)
export const CONNECTION = 'CONNECTION'
export const ERROR = 'ERROR'
export const LAST_EVENT = 'LAST_EVENT'
export const PROFILE = 'PROFILE'
export const CHAT = 'CHAT'
export const RESET = 'RESET'

// for game and play
export const GAME_JOIN = 'GAME_JOIN'
export const GAME_SYNC = 'GAME_SYNC'
export const PLAY_SYNC = 'PLAY_SYNC'
export const BALANCE_SYNC = 'BALANCE_SYNC'
