import { assign, mapValues, forEach } from 'lodash'

export class Style {
  constructor (theme, width, isMobileOrSmall = false) {
    this.theme = theme
    this.width = width

    this.states = {
      playing: [],
      cashed: [],
      lost: [],
      starting: [],
      startingBetting: ['starting', 'playing'],
      progress: [],
      progressPlaying: ['progress', 'playing'],
      progressCashed: ['progress', 'cashed'],
      ended: [],
      endedCashed: ['ended', 'cashed']
    }

    const strokeStyle = this.combineTheme({
      white: 'Black',
      black: '#b0b3c1'
    })

    const fillStyle = this.combineTheme({
      white: 'black',
      black: '#b0b3c1'
    })

    this.graph = this.combineState({
      base: {
        lineWidth: 4,
        strokeStyle: strokeStyle
      },
      playing: {
        lineWidth: 6,
        strokeStyle: '#2C2C2C'
      },
      cashed: {
        /* strokeStyle = "Grey", */
        lineWidth: 6
      }
    })

    if (this.theme === 'black') {
      this.graph.playing.strokeStyle = '#A47647'
    }

    this.axis = {
      lineWidth: 1,
      font: '10px Verdana',
      textAlign: 'center',
      strokeStyle,
      fillStyle
    }

    let fontSizes = {
      progress: 20,
      ended: 15
    }
    if (isMobileOrSmall) {
      fontSizes = {
        progress: 11,
        ended: 11
      }
    }

    this.data = this.combineState({
      base: {
        textAlign: 'center',
        textBaseline: 'middle'
      },
      starting: {
        font: `${this.fontSizePx(5)} Verdana`,
        fillStyle: 'grey'
      },
      progress: {
        font: `${this.fontSizePx(fontSizes.progress)} Verdana`,
        fillStyle
      },
      progressPlaying: {
        fillStyle: '#7cba00'
      },
      ended: {
        font: `${this.fontSizePx(fontSizes.ended)} Verdana`,
        fillStyle: 'red'
      }
    })

    return this
  }

  combineTheme (obj) {
    if (typeof obj[this.theme] === 'string') {
      return obj[this.theme]
    }
    return assign({}, obj.base, obj[this.theme])
  }

  combineState (obj) {
    return mapValues(this.states, (sups, state) => {
      const res = assign({}, obj.base || {})
      forEach(sups, (sup) => {
        assign(res, obj[sup] || {})
      })
      assign(res, obj[state])
      return res
    })
  }

  fontSizeNum (times) {
    return (times * this.width) / 100
  }

  fontSizePx (times) {
    const fontSize = this.fontSizeNum(times)
    return `${fontSize.toFixed(2)}px`
  }
}

export const XTICK_LABEL_OFFSET = 20
export const XTICK_MARK_LENGTH = 5
export const YTICK_LABEL_OFFSET = 11
export const YTICK_MARK_LENGTH = 5

export function tickSeparation (s) {
  let r = 1
  while (true) {
    if (r > s) { return r }
    r *= 2

    if (r > s) { return r }
    r *= 5
  }
}
